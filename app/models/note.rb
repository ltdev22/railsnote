class Note < ActiveRecord::Base

    # Associations
    belongs_to :user

    
    # Scopes
    scope :newest_first, -> {order("created_at DESC")}

    scope :by_user, lambda { |user| 
        where("notes.user_id = ?", user.id)
    }

    
    # Validations
    validates :title, 
                presence: true

    validates :content,
                presence: true,
                length: { minimum: 5 }
end
