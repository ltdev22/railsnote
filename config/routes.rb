Rails.application.routes.draw do
  
    devise_for :users
    resources :notes

    get 'welcome/index'

    authenticated :user do
        root "notes#index", as: "authnticated_root"
    end

    root to: "welcome#index"
  
end
